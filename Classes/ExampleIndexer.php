<?php
// set you own vendor name adjust the extension name part of the namespace to your extension key
namespace TeaminmediasPluswerk\KeSearchHooks;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Connection;

// set you own class name
class ExampleIndexer
{
    // Set a key for your indexer configuration.
    // Add this in Configuration/TCA/Overrides/tx_kesearch_indexerconfig.php, too!
    protected $indexerConfigurationKey = 'publishsystem';

    /**
     * Adds the custom indexer to the TCA of indexer configurations, so that
     * it's selectable in the backend as an indexer type when you create a
     * new indexer configuration.
     *
     * @param array $params
     * @param type $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj)
    {
        // Set a name and an icon for your indexer.
        $customIndexer = array(
            'PublishSystem Indexer',
            $this->indexerConfigurationKey,
            PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('ke_search_hooks')) . 'publishsystem-indexer-icon.gif'
        );
        $params['items'][] = $customIndexer;
    }

    protected function indexPublishTables(&$indexerConfig, &$indexerObject)
	{
		$cp = GeneralUtility::makeInstance(ConnectionPool::class);
		/** @var TYPO3\CMS\Core\Database\Connection $connection */
		$connection = $cp->getConnectionByName(reset($cp->getConnectionNames()));
		$return = [];
		
		// first, we need go get all tables
		// $sql = 'SHOW TABLES LIKE "publish\_%"';
		$sql = 'SELECT DATABASE() AS name';
		
		if($db_table = $connection->fetchAssoc($sql)){
			$db_name = $db_table['name'];
			
			$sql = 'SELECT 
						table_name AS tablename
					FROM information_schema.tables
					WHERE table_schema = "' . $db_name . '" 
					AND table_name LIKE "publish\_%"';
									
			// for each table we have found -> add the content to the index!
			$publishTables = $connection->fetchAll($sql);
			foreach($publishTables as $dbtable) {
				$resCount = 0;

				$sql = 'SELECT * FROM '.$dbtable['tablename'];
				$records = $connection->fetchAll($sql);
				foreach ($records as $record) {

					// compile the information which should go into the index
					// the field names depend on the table you want to index!
					$title = $record['typoTitle'];
					$abstract = $record['typoAbstract'];
					$content = $record['typoContent'];
					
					$fullContent = $title . '<br />' . $abstract . '<br />' . $content;
					$params = '&publish[id]='.$record['articleId'];
					
					// set tags
					$tags = '';
					if($dbtable['tablename'] == "publish_calendar" || $dbtable['tablename'] == "publish_tender" || $dbtable['tablename'] == "publish_news" || $dbtable['tablename'] == 'publish_emergency' || $dbtable['tablename'] == 'publish_hall' || $dbtable['tablename'] == 'publish_waste'){
						if($record['nonPublic']){
							$tags = '#news#';
						}else{
							$tags = '#news#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_geoobject" || $dbtable['tablename'] == "publish_club" || $dbtable['tablename'] == "publish_company" || $dbtable['tablename'] == 'publish_gastronomy' || $dbtable['tablename'] == 'publish_supplier' || $dbtable['tablename'] == 'publish_tourism' || $dbtable['tablename'] == 'publish_bauguide'){
						if($record['nonPublic']){
							$tags = '#geo#';
						}else{
							$tags = '#geo#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_servicebw" || $dbtable['tablename'] == "publish_baybw" || $dbtable['tablename'] == 'publish_photo' || $dbtable['tablename'] == 'publish_guestbook' || $dbtable['tablename'] == 'publish_reminder' || $dbtable['tablename'] == 'publish_shop' || $dbtable['tablename'] == 'publish_voting'){
						if($record['nonPublic']){
							$tags = '#dienst#';
						}else{
							$tags = '#dienst#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_participation" || $dbtable['tablename'] == "publish_problem"){
						if($record['nonPublic']){
							$tags = '#beteiligung#';
						}else{
							$tags = '#beteiligung#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_form"){
						if($record['nonPublic']){
							$tags = '#file#,#doc#';
						}else{
							$tags = '#file#,#doc#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_board"){
						$tags = '#board#,#public#';
					}
					if($dbtable['tablename'] == "publish_conference"){
						if($record['nonPublic']){
							$tags = '#conf#';
						}else{
							$tags = '#conf#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_conferenceFile"){
						if($record['nonPublic']){
							$tags = '#conf#';
						}else{
							$tags = '#conf#,#public#';
						}
					}
					if($dbtable['tablename'] == "publish_party"){
						$tags = '#party#,#public#';
					}
					if($dbtable['tablename'] == "publish_person"){
						$tags = '#person#,#public#';
					}

					$additionalFields = [
						'sortdate' => $record['typoDate'],
						'orig_uid' => $record['uid'],
						'orig_pid' => 0,
					];

					// add something to the title, just to identify the entries in the frontend
					$title = $title;

					// ... and store the information in the index
					$indexerObject->storeInIndex(
						$indexerConfig['storagepid'],  	// storage PID
						$title,                         // record title
						$dbtable['tablename'],           	// content type
						$record['typoUrl'],    			// target PID: where is the single view?
						$fullContent,                   // indexed content, includes the title (linebreak after title)
						$tags,                          // tags for faceted search
						$params,                        // typolink params for singleview
						$abstract,                      // abstract; shown in result list if not empty
						0,    							// language uid
						0,           					// starttime
						0,             					// endtime
						0,            					// fe_group
						false,                          // debug only?
						$additionalFields               // additionalFields
					);

					$resCount++;
				}
				
				$return[$dbtable['tablename']] = $resCount;
			}
		}
		
		return $return;
	}


    /**
     * Custom indexer for ke_search
     *
     * @param   array $indexerConfig Configuration from TYPO3 Backend
     * @param   array $indexerObject Reference to indexer class.
     * @return  string Message containing indexed elements
     * @author  Christian Buelter <christian.buelter@pluswerk.ag>
     */
    public function customIndexer(&$indexerConfig, &$indexerObject) {
		
		if($indexerConfig['type'] == 'publishsystem') {
			
			$publishIndexer = $this->indexPublishTables($indexerConfig, $indexerObject);
			
			$content = '';
			$content = $content . '<p>';
			$content = $content . '<b>Publish Indexer</b> indexed the following elements:<br>';
			
			foreach($publishIndexer as $key => $value) {
				$content = $content . ' -> ' . $key . ': ' . $value . ' elements<br>';
			}
			
			$content = $content . '</p>';
			
			return $content;
		}
	}

}