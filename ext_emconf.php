<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Publish Indexer',
    'description' => 'Publish System Search Indexer',
    'category' => 'backend',
    'version' => '11.5.0',
    'dependencies' => 'ke_search',
    'state' => 'stable',
    'author' => 'Kevin Andrews',
    'author_email' => 'kevin.andrews@cmcitymedia.de',
    'author_company' => 'cmc',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.5.0-11.99.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
